package schema;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ComplexElement extends BasicElement {

	public static final String Type = "Complex";
	
	private List<SchemaElement> elRefs;
	
	private List<SchemaElement> elements;

	public ComplexElement(String name) {
		super(name, Type);
		
		elRefs = new ArrayList<SchemaElement>();
	}
	
	/**
	 * Adds ElementRef instances.
	 * 
	 * @param element
	 */
	public void addElement(SchemaElement element) {
		elRefs.add(element);
	}

	/**
	 * Resolve references to Elements;
	 * 
	 * @param elMap
	 */
	public void resolveElements(Map<String, SchemaElement> elMap) {
		elements = new ArrayList<SchemaElement>();
		
		for (SchemaElement element : elRefs) {
			if (element instanceof ElementRef) {
				SchemaElement ref = elMap.get(((ElementRef) element).getRef());
				elements.add(ref);
			}
		}
	}
	
	public List<SchemaElement> getElements() {
		return elements;
	}

	public ComplexElement getFirst() {
		SchemaElement el = elements.get(0);
		if (el instanceof ComplexElement) {
			return (ComplexElement) el;
		}
		return null;
	}
	
}
