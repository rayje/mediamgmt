package servlets;

import helpers.GetHelper;
import helpers.ProfileHelper;
import helpers.RequestHelper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.Profile;
import exceptions.NoSchemaException;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/Profiles")
public class Profiles extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String PROFILES_JSP = "profiles.jsp";
	private static final String ERROR_MESSAGE = "errorMessage";
	private static final String MESSAGE = "message";
	private static final String PROFILES = "profiles";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProfileHelper helper = (ProfileHelper) request.getServletContext().getAttribute(ProfileHelper.SERVICE_NAME);
		
		forwardToJsp(request, response, helper);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProfileHelper helper = (ProfileHelper) request.getServletContext().getAttribute(ProfileHelper.SERVICE_NAME);
		
		Map<String, String>[] data = RequestHelper.extractData(request.getParameterMap());
		helper.addProfile(data);
		
		forwardToJsp(request, response, helper);
	}

	private void forwardToJsp(HttpServletRequest request,
			HttpServletResponse response, ProfileHelper helper)
			throws ServletException, IOException {
		GetHelper getHelper = (GetHelper) request.getServletContext().getAttribute(GetHelper.SERVICE_NAME);
		
		try {
			getHelper.getTagDescriptions(request);
		} catch (NoSchemaException e) {
			request.setAttribute(ERROR_MESSAGE, "You must first upload your schema files");
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
			dispatcher.forward(request, response);
			
			return;
		}
		
		List<Profile> profiles = helper.getProfiles();
		if (profiles.isEmpty()) {
			request.setAttribute(MESSAGE, "No Profiles have been created.");
		} else {
			request.setAttribute(PROFILES, profiles);
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(PROFILES_JSP);
		dispatcher.forward(request, response);
	}
}
