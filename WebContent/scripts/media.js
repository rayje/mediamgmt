$(function() {
	$( ".datepicker" ).datepicker();
	
	$("#addBtn").click(addTag);
	
	$("#tag_select").change(handleSelect);
});

function addTag(event) {
	var div = $("#tag_template").clone().removeClass('hidden');
	
	$(div).children('button').click(addTag);
	var sel = $(div).children('select')
	    		.change(handleSelect)
	    		.attr({'id':'tag_select'})
	    		.val('title');
	
	
	div.append($("<button>x</button>").addClass("smallBtn").click(removeTag));
	$('#tags').append(div);
	
	$(sel).val($("#tag_select option:first").val());
	var name = $(sel).find(":selected").val();
	$(div).children('input').attr({'name': name});
	
	return false;
}

function removeTag(event) {
	var parent = $(event.target).parent();
	parent.remove();
	
	return false;
}

function handleSelect(event) {
	$.ajax({
		url: 'Values',
		data: {'select':$(event.target).val()},
		dataType: 'json',
		success: function(data, status, jxhr) {
			var parent = $(event.target).parent();
			var name = $(parent).children('#tag_select').find(":selected").val();
			
			if (data['type'] == 'enum') {
				var sel = $('<select>')
					.addClass('select')
					.attr({'name':name, 'id': 'value_select'});
				$.each(data['values'], function(key, value) {
				 	sel.append($("<option>").attr('value',value).text(key));
				});
				
				var input = $(parent).children('input');
				$(input).before(sel);
				input.remove();
			} else {
				var sel = $(parent).find('#value_select');
				if (sel !== undefined) {
					var input = $('<input/>').attr({'class': 'input', 'type': 'text', 'name': name, 'size': '50'});
					sel.before(input);
					sel.remove();
				}
				
				if (data['type'] == 'Timestamp') {
					$(parent).children('input')
						.addClass('datepicker')
						.datepicker()
						.attr({'name':name});
				} else {
					$(parent).children('input')
						.datepicker("destroy")
						.removeClass('datepicker')
						.attr({'name': name});
				}
			}
		}
	});
}