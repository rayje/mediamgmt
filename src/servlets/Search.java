package servlets;

import helpers.GetHelper;
import helpers.ProfileHelper;
import helpers.SearchHelper;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.Media;
import database.Profile;
import exceptions.NoSchemaException;

/**
 * Servlet implementation class Search
 */
@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String PAGE_HEADER = "page_header";
	private static final String ERROR_MESSAGE = "errorMessage";
	private static final String PROFILES = "profiles";
	private static final String MESSAGE = "message";
	private static final String SEARCH_JSP = "search.jsp";
	private static final String ADMIN_JSP = "admin.jsp";
	private static final String RESULTS_JSP = "results.jsp";
	private static final String MEDIA_ITEMS = "mediaItems";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProfileHelper helper = (ProfileHelper) request.getServletContext().getAttribute(ProfileHelper.SERVICE_NAME);
		
		List<Profile> profiles = helper.getProfiles();
		if (profiles.isEmpty()) {
			request.setAttribute(MESSAGE, "No Profiles have been created.");
		} else {
			request.setAttribute(PROFILES, profiles);
		}
		
		GetHelper getHelper = (GetHelper) request.getServletContext().getAttribute(GetHelper.SERVICE_NAME);
		try {
			getHelper.getTagDescriptions(request);
		} catch (NoSchemaException e) {
			request.setAttribute(ERROR_MESSAGE, "You must first upload your schema files");
			
			RequestDispatcher dispatcher = request.getRequestDispatcher(ADMIN_JSP);
			dispatcher.forward(request, response);
			
			return;
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(SEARCH_JSP);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SearchHelper helper = (SearchHelper) request.getServletContext().getAttribute(SearchHelper.SERVICE_NAME);
		
		Collection<Media> media = helper.search(request);
		
		request.setAttribute(MEDIA_ITEMS, media);
		request.setAttribute(PAGE_HEADER, "Search results");
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(RESULTS_JSP);
		dispatcher.forward(request, response);
	}


}
