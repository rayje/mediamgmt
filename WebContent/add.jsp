<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
			<meta charset='utf-8'>
			<title>Project 8 (Add)</title>
			
			<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
			<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
			<script type="text/javascript" src="scripts/media.js"></script>
			
			<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
			<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<%@include file="header.jsp"%>
		
		<div class="hidden" id="tag_template">
			<select name="tag_name" class="select">
				<c:forEach items="${tagDescriptions}" var="tagDesc">
					<option value="${ tagDesc['tablename'] }:${ tagDesc['name'] }">${ tagDesc['name'] }</option>
				</c:forEach>
			</select>
			<input class="input" type="text" name="tag" size="50"/>
			<button class="smallBtn">+</button>
		</div>
	
		<div class="body">
			<div class="page">
				<div class="page-header">
					<span>Add Media</span>
				</div>
				
				<form name="upload" action="Add" class="add-form" enctype="multipart/form-data" method="post">
					<div class="inner-container data-item">
						<span class="container-header">Media File</span>
						<hr/>
						<p>
							Upload File: <input type="file" name="media_file" />
						</p>
						<br/>
					</div>
					
					<div class="inner-container data-item">
						<span class="container-header">Tags</span>
						<hr/>
						<div id="tags">
							<div>
								<select id="tag_select" name="tag_name" class="select" id="main-select">
									<c:forEach items="${tagDescriptions}" var="tagDesc">
										<option value="${ tagDesc['tablename'] }:${ tagDesc['name'] }">${ tagDesc['name'] }</option>
									</c:forEach>
								</select>
								<input class="input" type="text" name="${first}" size="50" id="main-input"/>
								<button class="smallBtn" id="addBtn">+</button>
							</div>
						</div>
					</div>
					
					<div style="margin-top: 15px;">
						<p>
							<input type="submit" value="Submit" class="btn"/>
						</p>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>