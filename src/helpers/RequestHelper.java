package helpers;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Part;

import database.Media;

public class RequestHelper {

	@SuppressWarnings("unchecked")
	public static Map<String, String>[] extractData(Map<String, String[]> paramsMap) {
		Map<String, String> standardData = new HashMap<String, String>();
		Map<String, String> domainData = new HashMap<String, String>();
		
		for (Map.Entry<String, String[]> entry : paramsMap.entrySet()) {
			String key = entry.getKey();
			String value[] = entry.getValue();
			String keyParts[] = key.split(":");
			
			if (keyParts.length != 2) {
				continue;
			}
			
			if (value.length > 0 && value[0].length() > 0) {
				if (keyParts[0].toUpperCase().equals(AddHelper.DOMAIN.toUpperCase())) {
					domainData.put(keyParts[1], value[0]);
				} else {
					standardData.put(keyParts[1], value[0]);
				}
			}
		}
		
		Map<String, String>[] dataMaps = new HashMap[2];
		dataMaps[0] = standardData;
		dataMaps[1] = domainData;
		
		return dataMaps;
	}
	
	public static Media addData(Part mediaPart, Map<String, String[]> paramsMap, AddHelper helper) throws IOException {
		Map<String, String>[] data = extractData(paramsMap);
		
		InputStream mediaStream = mediaPart.getInputStream();
		String filename = getFilename(mediaPart);
		
		Media media = helper.addMedia(mediaStream, filename, data[0], data[1]);
		
		return media;
	}
	
	public static String getFilename(Part p) {
		String h = p.getHeader("content-disposition");
		String[] sections = h.split("\\s*;\\s*");
		
		for (String s : sections) {
			if (s.startsWith("filename=")) {
				return s.substring(9).replace("\"", "");
			}
		}
		
		return null;
	}
	
	public static void updateData(String mediaId, Map<String, String[]> paramsMap, AddHelper helper) throws IOException {
		Map<String, String>[] data = extractData(paramsMap);
		
		helper.updateMedia(mediaId, data[0], data[1]);
	}
	
	
}
