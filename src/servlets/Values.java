package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import database.Column;
import database.Database;
import database.ForeignColumn;
import database.Table;

/**
 * Servlet implementation class Values
 */
@WebServlet("/Values")
public class Values extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Values() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String select = request.getParameter("select");
		String[] parts = select.split(":");
		
		Database db = (Database) request.getServletContext().getAttribute(Database.SERVICE_NAME);
		Column col = null;
		try {
			Table table = db.getTable(parts[0]);
			col = table.getColumn(parts[1]);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		JSONObject message = new JSONObject();
		if (col instanceof ForeignColumn) {
			message.put("type", "enum");
			message.put("values", ((ForeignColumn) col).getValues());
		} else {
			message.put("type", col.getType());
		}
		
		response.getWriter().print(message.toString());
		response.getWriter().close();
	}


}
