package schema;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import database.Table;

public class XsdParser {

	public static final String DDL = "ddl";
	public static final String INSERTS = "inserts";
	
	private static final String XSD_SEQUENCE = "xsd:sequence";
	private static final String REF = "ref";
	private static final String VALUE = "value";
	private static final String BASE = "base";
	private static final String TYPE = "type";
	private static final String NAME = "name";
	private static final String XSD_ELEMENT = "xsd:element";
	private static final String XSD_ENUMERATION = "xsd:enumeration";
	private static final String XSD_RESTRICTION = "xsd:restriction";
	private static final String XSD_COMPLEX_TYPE = "xsd:complexType";
	private static final String XSD_SIMPLE_TYPE = "xsd:simpleType";
	private static final String XSD_ALL = "xsd:all";
	
	private Map<String, SchemaElement> elements;

	public static void main(String[] args) throws FileNotFoundException {
		InputStream is = new FileInputStream(new File("standard.xsd"));
		
		XsdParser parser = new XsdParser();
		Map<String, List<String>> ddlMap = parser.parseToDdl(is, Table.STANDARD);

		List<String> ddl = ddlMap.get(DDL);
		for (String sql : ddl) {
			System.out.println(sql);
			System.out.println("-----------------------------------");
		}
		
		List<String> inserts = ddlMap.get(INSERTS);
		for (String sql : inserts) {
			System.out.println(sql);
			System.out.println("-----------------------------------");
		}
	}
	
	public Map<String, List<String>> parseToDdl(InputStream is, String type) {
		parse(is);
		Map<String, List<String>> ddl = compileSql(type);
		
		return ddl;
	}
	
	public Map<String, List<String>> compileSql(String type) {
		List<ComplexElement> complex = new ArrayList<ComplexElement>();
		List<SimpleElement> simple = new ArrayList<SimpleElement>();
		
		for (Map.Entry<String, SchemaElement> entry : elements.entrySet()) {
			SchemaElement element = entry.getValue();
			
			if (element instanceof ComplexElement) {
				((ComplexElement) element).resolveElements(elements);
				
				complex.add((ComplexElement) element);
			} else if (element instanceof SimpleElement) {
				simple.add((SimpleElement) element);
			}
		}
		
		elements = null;
		
		Map<String, List<String>> ddl = new HashMap<String, List<String>>();
		
		for (ComplexElement c : complex) {
			String name = c.getName();
			if (name.equals("standard") || name.equals("domain")) {
				compileSql(c, ddl, type);
			}
		}
		
		return ddl;
	}
	
	public void compileSql(ComplexElement ce, Map<String, List<String>> ddlMap, String type) {
		ComplexElement first = ce.getFirst();
		if (first == null)
			return;
		
		List<SchemaElement> elements = first.getElements();
		List<SimpleElement> simpleElements = new ArrayList<SimpleElement>();
		
		String sql = createMainSql(ce, elements, simpleElements, type);
		
		List<String> refSql = new ArrayList<String>();
		List<String> refInserts = new ArrayList<String>();
		createRefSql(simpleElements, refSql, refInserts);
		
		List<String> ddl = new ArrayList<String>();
		ddl.addAll(refSql);
		ddl.add(sql);
		
		ddlMap.put(INSERTS, refInserts);
		ddlMap.put(DDL, ddl);
	}

	private void createRefSql(List<SimpleElement> simpleElements, List<String> refSql, List<String> refInserts) {
		for (SimpleElement simple : simpleElements) {
			StringBuilder simpleSql = new StringBuilder();
			simpleSql.append("CREATE TABLE ");
			simpleSql.append(simple.getName().toUpperCase());
			simpleSql.append(" (\n");
			simpleSql.append("  ID SERIAL PRIMARY KEY,\n");
			simpleSql.append("  ");
			simpleSql.append(simple.getName());
			simpleSql.append(" VARCHAR\n");
			simpleSql.append(")");
			
			StringBuilder simpleInsert = new StringBuilder();
			simpleInsert.append("INSERT INTO ");
			simpleInsert.append(simple.getName().toUpperCase());
			simpleInsert.append(" (");
			simpleInsert.append(simple.getName());
			simpleInsert.append(")");
			simpleInsert.append(" VALUES ");
			
			StringBuilder values = new StringBuilder();
			List<Enumeration> enumerations = simple.getEnumerations();
			for (Enumeration e : enumerations) {
				if (values.length() > 0) {
					values.append(", ");
				}
				
				StringBuilder value = new StringBuilder();
				value.append("('");
				value.append(e.getValue());
				value.append("')");
				
				values.append(value);
			}
			
			simpleInsert.append(values);
			
			refSql.add(simpleSql.toString());
			refInserts.add(simpleInsert.toString());
		}
	}

	private String createMainSql(ComplexElement ce, List<SchemaElement> elements, List<SimpleElement> simpleElements, String type) {
		StringBuilder sql = new StringBuilder();
		sql.append("CREATE TABLE ");
		sql.append(type.toUpperCase());
		sql.append(" (");
		sql.append("\n");
		sql.append("  ID SERIAL PRIMARY KEY,\n");
		
		StringBuilder cols = new StringBuilder();
		for (SchemaElement element : elements) {
			if (cols.length() > 0) {
				cols.append(",\n");
			}
			
			StringBuilder col = new StringBuilder();
			if (element instanceof SimpleElement) {
				SimpleElement se = (SimpleElement) element;
				String name = se.getName();

				col.append("  ");
				col.append(name);
				col.append(" ");
				col.append("INTEGER REFERENCES ");
				col.append(name);
				
				simpleElements.add(se);
			} else {
				BasicElement be = (BasicElement) element;
				col.append("  ");
				col.append(be.getName());
				col.append(" ");
				col.append(be.getType());
			}
			
			cols.append(col);
		}
		
		if (type.equals(Table.STANDARD)) {
			cols.append(",\n");
			cols.append("  mediaid VARCHAR");
		} else if (type.equals(Table.DOMAIN)) {
			cols.append(",\n");
			cols.append("  standard_id INTEGER REFERENCES ");
			cols.append(Table.STANDARD);
		}
		
		sql.append(cols);
		sql.append("\n)");
		return sql.toString();
	}
	
	public void parse(InputStream is) {
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder;
			docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(is);
			NodeList list = doc.getChildNodes().item(0).getChildNodes();
			
			elements = new HashMap<String, SchemaElement>();

			// loop to print data
			for (int i = 0; i < list.getLength(); i++) {
				Node n = list.item(i);
				if (n instanceof Element)
					parseElement((Element) list.item(i));	
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException ed) {
			ed.printStackTrace();
		}
	}

	private void parseElement(Element el) {
		String name = el.getAttribute(NAME);
		
		if (el.hasChildNodes()) {
			SchemaElement e = processElementWithChildren(el, name);
			elements.put(name, e);
		} else {
			SchemaElement e = createElement(el, name);
			elements.put(name, e);
		}
	}
	
	private SchemaElement createElement(Element el, String name) {
		String tagType = el.getTagName();
		
		if (tagType.equals(XSD_ELEMENT) && el.hasAttribute(TYPE)) {
			
			return new BasicElement(name, el.getAttribute(TYPE));
			
		} else if (el.hasAttribute(VALUE)) {
			
			if (tagType.equals(XSD_ENUMERATION)) {
				String value = el.getAttribute(VALUE);
				return new Enumeration(value);
			}
			
		}
		
		return null;
	}

	private SchemaElement processElementWithChildren(Element el, String name) {
		NodeList nodes = el.getChildNodes();
		
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (!(n instanceof Element))
				continue;
			
			Element nodeEl = (Element) n;
			String tagName = nodeEl.getTagName();
			
			if (tagName.equals(XSD_COMPLEX_TYPE)) {
				
				return processComplexElement(nodeEl, name);
				
			} else if (tagName.equals(XSD_SIMPLE_TYPE)) {
				
				return processSimpleElement(nodeEl, name);
				
			}
			
		}
		
		return null;
	}
	
	private SchemaElement processComplexElement(Element el, String name) {
		NodeList nodes = el.getChildNodes();
		ComplexElement complexEl = null;
		
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (!(n instanceof Element))
				continue;
			
			Element nodeEl = (Element) n;
			String tagName = nodeEl.getTagName();
	
			complexEl = new ComplexElement(name);
			
			if (tagName.equals(XSD_ALL) || tagName.equals(XSD_SEQUENCE)) {
				
				NodeList refs = nodeEl.getChildNodes();
				
				for (int j = 0; j < refs.getLength(); j++) {
					Node n2 = refs.item(j);
					if (!(n2 instanceof Element))
						continue;
					
					Element e = (Element) n2;
					String ref = e.getAttribute(REF);
					
					ElementRef elRef = new ElementRef(ref);
					complexEl.addElement(elRef);
				}
				
			}
		}
		
		return complexEl;
	}
	
	private SchemaElement processSimpleElement(Element el, String name) {
		NodeList nodes = el.getChildNodes();
		SimpleElement simpleEl = null;
		
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (!(n instanceof Element))
				continue;
		
			Element first = (Element) n;
			String tagName = first.getTagName();
			
			if (tagName.equals(XSD_RESTRICTION)) {
				simpleEl = new SimpleElement(name);
				
				String base = first.getAttribute(BASE);
				simpleEl.setBase(base);
				
				NodeList nodes2 = first.getChildNodes();
				for (int j = 0; j < nodes2.getLength(); j++) {
					Node n2 = nodes2.item(j);
					if (!(n2 instanceof Element)) 
						continue;
					
					Element e = (Element) n2;
					
					if (e.getTagName().equals(XSD_ENUMERATION)) {
						String value = e.getAttribute(VALUE);
						
						Enumeration en = new Enumeration(value);
						simpleEl.addEnumeration(en);
					}
				}
				
			}
		
		}
		
		return simpleEl;
	}
}
