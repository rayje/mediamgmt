package schema;

public class ElementRef implements SchemaElement {
	
	private String ref;
	
	public ElementRef(String ref) {
		this.ref = ref;
	}
	
	public String getRef() {
		return ref;
	}
}
