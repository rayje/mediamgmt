package database;

import java.sql.Types;

public class ParentColumn implements Column {


	private String name;
	private int type;
	private int size;
	private int nullable;
	private String tableName;
	
	public ParentColumn(String name, int dataType, int size, int nullable, String tableName) {
		this.name = name;
		this.type = dataType;
		this.size = size;
		this.nullable = nullable;
		this.tableName = tableName;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		switch(type){
		case Types.INTEGER:
			return Column.INTEGER;
		case Types.VARCHAR:
			return Column.STRING;
		case Types.TIMESTAMP:
			return Column.TIMESTAMP;
		default:
			return Column.STRING;
		}
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public int getSize() {
		return size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}
	
	public int getNullable() {
		return nullable;
	}
	
	public void setNullable(int nullable) {
		this.nullable = nullable;
	}

	public String getTablename() {
		return tableName;
	}
	
}
