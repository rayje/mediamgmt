package helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import database.Media;
import database.Profile;

/**
 * The SearchHelper is a controller that routes a search request to the
 * appropriate search methods.
 */
public class SearchHelper {

	public static final String SERVICE_NAME = "SearchHelper";

	private static final String PROFILE = "profile";
	private static final String SEARCH_TEXT = "search-text";
	private static final String TAG_VALUE = "tagValue";
	private static final String TAG_NAME = "tagName";
	private static final String FULLTEXT = "fulltext";
	private static final String MEDIAID = "mediaid";
	private static final String GROUP = "group";
	private static final String TAG = "tag";
	private static final String SEARCH_TYPE = "search-type";

	/**
	 * Performs a search based on the search type found in the request.
	 * 
	 * @param request
	 *            An instance of HttpServletRequest.
	 * @return A Collection of Media objects representing the media found in the
	 *         search.
	 */
	public Collection<Media> search(HttpServletRequest request) {
		GetHelper helper = (GetHelper) request.getServletContext()
				.getAttribute(GetHelper.SERVICE_NAME);
		String type = (String) request.getParameter(SEARCH_TYPE);

		Collection<Media> media = null;

		if (type.equals(TAG)) {
			media = searchByTag(request, helper);
		} else if (type.equals(GROUP)) {
			media = searchByGroup(request, helper);
		} else if (type.equals(MEDIAID)) {
			media = searchByMediaId(request, helper);
		} else if (type.equals(FULLTEXT)) {
			media = searchByFullText(request, helper);
		}

		return media;
	}

	/**
	 * Performs a Search by Tag.
	 * 
	 * @param request
	 *            An instance of HttpServletRequest.
	 * @param helper
	 *            An instance of GetHelper.
	 * @return A Collection of Media objects representing the media found in the
	 *         search.
	 */
	private Collection<Media> searchByTag(HttpServletRequest request,
			GetHelper helper) {
		String tagName = request.getParameter(TAG_NAME);
		String tagValue = request.getParameter(TAG_VALUE);

		return helper.getMediaByTag(tagName, tagValue);
	}

	/**
	 * Performs a Search by Group.
	 * 
	 * @param request
	 *            An instance of HttpServletRequest.
	 * @param helper
	 *            An instance of GetHelper.
	 * @return A Collection of Media objects representing the media found in the
	 *         search.
	 */
	private Collection<Media> searchByGroup(HttpServletRequest request,
			GetHelper getHelper) {
		ProfileHelper profileHelper = (ProfileHelper) request
				.getServletContext().getAttribute(ProfileHelper.SERVICE_NAME);

		String profileId = request.getParameter(PROFILE);
		Profile profile = profileHelper.getProfile(profileId);

		return getHelper.getMediaByProfile(profile);
	}

	/**
	 * Performs a Search by Media Id.
	 * 
	 * @param request
	 *            An instance of HttpServletRequest.
	 * @param helper
	 *            An instance of GetHelper.
	 * @return A Collection of Media objects representing the media found in the
	 *         search.
	 */
	private Collection<Media> searchByMediaId(HttpServletRequest request,
			GetHelper helper) {
		String mediaid = request.getParameter(MEDIAID);

		return helper.getMediaById(mediaid);
	}

	/**
	 * Performs a Full Text Search.
	 * 
	 * @param request
	 *            An instance of HttpServletRequest.
	 * @param helper
	 *            An instance of GetHelper.
	 * @return A Collection of Media objects representing the media found in the
	 *         search.
	 */
	private Collection<Media> searchByFullText(HttpServletRequest request,
			GetHelper helper) {
		String[] searchText = request.getParameterValues(SEARCH_TEXT);
		List<String> searchList = new ArrayList<String>();

		for (String text : searchText) {
			if (text.length() > 0) {
				searchList.add(text);
			}
		}

		return helper.getMediaByFullTextSearch(searchList);
	}

}
