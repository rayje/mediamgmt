package helpers;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import database.Column;
import database.Database;
import database.ForeignColumn;
import database.Media;
import database.MediaValue;
import database.Profile;
import database.Table;
import exceptions.NoSchemaException;

public class GetHelper {

	public static final String SERVICE_NAME = "GetHelper";
	
	private final Database db;
	
	public GetHelper(Database db) {
		this.db = db;
	}
	
	public void getMediaFile(OutputStream out, String id) throws IOException {
		db.retrieveFile(out, id);
	}
	
	public Collection<Media> getMediaByTag(String tagName, String tagValue) {
		try {
			String[] parts = tagName.split(":");
			if (parts.length != 2) {
				return Collections.emptyList();
			}
			
			Table table = db.getTable(parts[0]);
			if (table == null) {
				return Collections.emptyList();
			}
			
			Column col = table.getColumn(parts[1]);
			if (col == null) {
				return Collections.emptyList();
			}
			
			StringBuilder sql = new StringBuilder();
			sql.append("select mediaid from standard");

			if (!table.getName().toUpperCase()
					.equals(AddHelper.STANDARD.toUpperCase())) {
				sql.append(", ");
				sql.append(table.getName());
			}

			sql.append(" where ");
			sql.append(parts[1]);
			sql.append(" = ");
			if (col.getType().equals("String")) {
				sql.append("'");
				sql.append(tagValue);
				sql.append("'");
			} else {
				sql.append(tagValue);
			}
			
			System.out.println(sql.toString());
			
			return getMediaItems(sql.toString());
		} catch (SQLException e) {
			throw new RuntimeException(e.getCause());
		}
	}
	
	/**
	 * Queries the database for media id and filename and returned all values
	 * that match the query.
	 * 
	 * @param sql The SQL query to be used for the search.
	 * @return A Collection of Media instances.
	 */
	public Collection<Media> getMediaItems(String sql) {
		Statement stmt = null;
		List<Media> mediaItems = new ArrayList<Media>();
		try {
			stmt = db.getStatement();
			
			ResultSet rs = stmt.executeQuery(sql.toString());
			while (rs.next()) {
				String mediaid = rs.getString(1);
				String filename = db.retrieveFilename(mediaid);
				
				if (filename != null) {
					mediaItems.add(new Media(mediaid, filename));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getCause());
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return mediaItems;
	}

	public Collection<Media> getMediaById(String mediaid) {
		String filename = db.retrieveFilename(mediaid);
		
		if (filename != null) {
			return Collections.singletonList(new Media(mediaid, filename));
		}
		
		return Collections.emptyList();
	}

	public Collection<Media> getMediaByFullTextSearch(List<String> searchList) {
		StringBuilder sql = new StringBuilder();
		sql.append("select mediaid from standard, domain where description @@ ");
		
		StringBuilder list = new StringBuilder();
		for (String text : searchList) {
			if (list.length() > 0) {
				list.append(" & ");
			}
			list.append(text);
		}
		
		sql.append("'");
		sql.append(list);
		sql.append("' ");
		sql.append("and standard_id = standard.id");
		
		System.out.println(sql.toString());
		
		return getMediaItems(sql.toString());
	}
	
	public Media find(String mediaId) {
		Statement stmt = null;
		Media media = null;
		try {
			stmt = db.getStatement();
			
			String sql = "select * from standard where mediaid = '" + mediaId + "'";
			ResultSet rs = stmt.executeQuery(sql.toString());
			while (rs.next()) {
				ResultSetMetaData md = rs.getMetaData();
				int numCols = md.getColumnCount();
				media = new Media(mediaId, null);
				
				int standard_id = rs.getInt("id");
				setMediaValues(AddHelper.STANDARD, media, numCols, md, rs);
				
				sql = "select * from domain where standard_id = " + standard_id;
				rs = stmt.executeQuery(sql.toString());
				while (rs.next()) {
					md = rs.getMetaData();
					numCols = md.getColumnCount();
					setMediaValues(AddHelper.DOMAIN, media, numCols, md, rs);
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e.getCause());
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return media;
	}
	
	private void setMediaValues(String tablename, Media media, int numCols, ResultSetMetaData md, ResultSet rs) throws SQLException {
		Table table = db.getTable(tablename);
		
		// Set media values
		for (int i = 1; i <= numCols; i++) {
			String colName = md.getColumnName(i);
			if (colName.length() == 0 || Table.NAMES.contains(colName)) {
				continue;
			}
			
			int type = md.getColumnType(i);
			
			if (type == Types.INTEGER) {
				int value = rs.getInt(i);
				if (value < 0) {
					continue;
				}
				
				MediaValue mediaValue = new MediaValue(colName, null, "Integer", tablename);
				
				Column col = table.getColumn(colName);
				if (col instanceof ForeignColumn) {
					Map<String, Integer> mediaValues = ((ForeignColumn) col).getValues();
					mediaValue.setAllowedValues(mediaValues);
					mediaValue.setType("Enum");

					for (Map.Entry<String, Integer> entry : mediaValues.entrySet()) {
						String key = entry.getKey();
						Integer intValue = entry.getValue();
						if (value == intValue) {
							mediaValue.setValue(key);
						}
					}
				} else {
					mediaValue.setValue(String.valueOf(value));
				}
				
				media.addValue(mediaValue);
			} else if (type == Types.VARCHAR) {
				String value = rs.getString(i);
				if (value == null || value.length() == 0) {
					continue;
				}
				media.addValue(new MediaValue(colName, value, "String", tablename));
			} else if (type == Types.TIMESTAMP) {
				Timestamp ts = rs.getTimestamp(i);
				if (ts == null) {
					continue;
				}
				String value = new SimpleDateFormat("MM/dd/yyyy").format(ts);
				media.addValue(new MediaValue(colName, value, "Timestamp", tablename));
			}
		}
	}
	
	public void getTagDescriptions(HttpServletRequest request) throws NoSchemaException {
		try {
			Table standard = db.getTable(AddHelper.STANDARD);
			if (standard == null) {
				throw new NoSchemaException();
			}
			
			Table domain = db.getTable(AddHelper.DOMAIN);
			
			List<Column> columns = new ArrayList<Column>();
			
			for (Column col : standard.getColumns()) {
				if (!Table.NAMES.contains(col.getName())) {
					columns.add(col);
				}
			}
			
			for (Column col : domain.getColumns()) {
				if (!Table.NAMES.contains(col.getName())) {
					columns.add(col);
				}
			}
			
			Column first = columns.get(0);
			request.setAttribute("first", first.getTablename()+":"+first.getName());
			request.setAttribute("tagDescriptions", columns);
		} catch (SQLException e) {
			throw new RuntimeException(e.getCause());
		}
	}

	public Collection<Media> getMediaByProfile(Profile profile) {
		try {
			List<String> tables = new ArrayList<String>();
			tables.add("standard");
			
			StringBuilder sql = new StringBuilder();
			sql.append("select mediaid from ");

			
			
			StringBuilder values = new StringBuilder();
			for (Entry<String, Object> entry : profile.entrySet()) {
				StringBuilder value = new StringBuilder();
				String key = entry.getKey();
				if (key.equals("_id"))
					continue;
				String entryValue = (String) entry.getValue();
				
				String[] parts = key.split(":");
				Table table = db.getTable(parts[0]);
				if (!(tables.contains(table.getName()))) {
					tables.add(table.getName());
				}
				Column col = table.getColumn(parts[1]);
				
				value.append(col.getName());
				value.append(" = ");
				if (col.getType().equals("String")) {
					value.append("'");
					value.append(entryValue);
					value.append("'");
				} else {
					value.append(entryValue);
				}
				
				if (values.length() > 0) {
					values.append(" and ");
				}
				
				values.append(value);
			}
			
			StringBuilder fromTables = new StringBuilder();
			for (String table : tables ) {
				if (fromTables.length() > 0) {
					fromTables.append(", ");
				}
				fromTables.append(table);
			}
			
			sql.append(fromTables);
			sql.append(" where ");
			sql.append(values);
			
			System.out.println(sql.toString());
			
			return getMediaItems(sql.toString());
		} catch (SQLException e) {
			throw new RuntimeException(e.getCause());
		}
	}
	
}
