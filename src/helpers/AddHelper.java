package helpers;

import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Map;

import org.bson.types.ObjectId;

import database.Database;
import database.Media;
import database.Table;

public class AddHelper {

	private static final String MEDIA_ID = "mediaid";

	public static final String SERVICE_NAME = "AddHelper";
	
	public static final String STANDARD = "standard";
	
	public static final String DOMAIN = "domain";
	
	private final Database db;
	
	public AddHelper(Database db) {
		this.db = db;
	}
	
	public void updateMedia(String mediaId, Map<String, String> standardData, Map<String, String> domainData) {
		try {
			int id = updateStandardData(mediaId, standardData);
			
			updateDomainData(String.valueOf(id), domainData);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public Media addMedia(InputStream mediaStream, String filename, Map<String, String> standardData, Map<String, String> domainData) {
		String mediaId = (String) addMediaFile(mediaStream, filename);
		
		try {
			standardData.put(MEDIA_ID, mediaId);
			
			int id = addStandardData(standardData);
			
			domainData.put("standard_id", String.valueOf(id));
			addDomainData(domainData);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return new Media(mediaId, filename);
	}
	
	public String addMediaFile(InputStream mediaStream, String filename) {
		ObjectId id = (ObjectId) db.storeFile(mediaStream, filename);
		return id.toString();
	}
	
	public int addStandardData(Map<String, String> data) throws SQLException, ParseException {
		Table table = db.getTable(STANDARD);
		return table.insert(data);
	}
	
	public void addDomainData(Map<String, String> data) throws SQLException, ParseException {
		Table table = db.getTable(DOMAIN);
		table.insert(data);
	}
	
	public int updateStandardData(String mediaId, Map<String, String> data) throws SQLException, ParseException {
		Table table = db.getTable(STANDARD);
		return table.update(mediaId, data, STANDARD);
	}
	
	public void updateDomainData(String standardId, Map<String, String> data) throws SQLException, ParseException {
		Table table = db.getTable(DOMAIN);
		table.update(standardId, data, DOMAIN);
	}
	
}
