package servlets;

import helpers.ProfileHelper;
import helpers.RemoveHelper;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Delete
 */
@WebServlet("/Delete")
public class Delete extends HttpServlet {
	private static final String RESULTS_JSP = "results.jsp";

	private static final String PROFILE = "profile";

	private static final String ID = "id";

	private static final String MESSAGE = "message";

	private static final long serialVersionUID = 1L;
	
	private static final String DELETE_RESULTS = "Delete results";
	private static final String PAGE_HEADER = "page_header";
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RemoveHelper helper = (RemoveHelper) request.getServletContext().getAttribute(RemoveHelper.SERVICE_NAME);
		
		String profile = null;
		String id = request.getParameter(ID);
		if (id == null) {
			profile = request.getParameter(PROFILE);
		}
		
		try {
			if (id != null) {
				helper.remove(id);
				request.setAttribute(MESSAGE, "Media: " + id + " removed");
			} else {
				ProfileHelper profileHelper = (ProfileHelper) request.getServletContext().getAttribute(ProfileHelper.SERVICE_NAME);	
				int removed = profileHelper.removeProfile(profile);
				
				request.setAttribute(MESSAGE, removed + " Profile(s) removed");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			request.setAttribute(MESSAGE, "Error removing: " + id);
		}
		
		request.setAttribute(PAGE_HEADER, DELETE_RESULTS);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(RESULTS_JSP);
		dispatcher.forward(request, response);
	}

}
