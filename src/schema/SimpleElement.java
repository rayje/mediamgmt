package schema;

import java.util.ArrayList;
import java.util.List;

public class SimpleElement implements SchemaElement {

	private String name;
	
	private String base;
	
	private List<Enumeration> enumerations;
	
	public SimpleElement(String name) {
		this.name = name;
		
		enumerations = new ArrayList<Enumeration>();
	}
	
	public void setBase(String base) {
		this.base = base;
	}
	
	public String getBase() {
		return base;
	}
	
	public String getName() {
		return name;
	}
	
	public void addEnumeration(Enumeration e) {
		enumerations.add(e);
	}
	
	public List<Enumeration> getEnumerations() {
		return enumerations;
	}

}
