package database;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class MediaValue {

	private String name;
	private String value;
	private String type;
	private String tablename;
	private List<String> allowedValues;
	
	public MediaValue(String name, String value, String type, String tablename) {
		this.name = name;
		this.value = value;
		this.type = type;
		this.tablename = tablename;
	}
	
	public void setAllowedValues(Map<String, Integer> map) {
		allowedValues = new ArrayList<String>(map.size() + 1);
		for (int i = 0; i < map.size() + 1; i++) {
			allowedValues.add("0");
		}
		
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			String key = entry.getKey();
			Integer value = entry.getValue();
			
			System.out.println("Setting " + value + ": " + key);
			allowedValues.set(value, key);
		}
	}
	
	public Collection<String> getAllowedValues() {
		return allowedValues;
	}
	
	public Integer getIndexValue() {
		if (allowedValues != null && value != null) {
			return allowedValues.indexOf(value);
		}
		
		return null;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTablename() {
		return tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	
	
	
}
