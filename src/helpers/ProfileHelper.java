package helpers;

import java.util.List;
import java.util.Map;

import database.Database;
import database.Profile;
import database.Table;

public class ProfileHelper {

	public static final String SERVICE_NAME = "ProfileHelper";
	private Database db;
	
	public ProfileHelper(Database db) {
		this.db = db;
	}
	
	public List<Profile> getProfiles() {
		return db.getProfiles();
	}
	
	public void addProfile(Map<String, String>[] data) {
		Profile profile = new Profile();
		
		Map<String, String> stdData = data[0];
		Map<String, String> domData = data[1];
		
		for (Map.Entry<String, String> entry : stdData.entrySet()) {
			profile.put(Table.STANDARD + ":" + entry.getKey(), entry.getValue());
		}
		
		for (Map.Entry<String, String> entry : domData.entrySet()) {
			profile.put(Table.DOMAIN + ":" + entry.getKey(), entry.getValue());
		}
		
		db.addProfile(profile);
	}
	
	public Profile getProfile(String id) {
		return db.getProfile(id);
	}

	public int removeProfile(String id) {
		return db.removeProfile(id);
		
	}
	
}
