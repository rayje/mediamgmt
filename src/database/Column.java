package database;

public interface Column {

	public static final String TIMESTAMP = "Timestamp";
	public static final String STRING = "String";
	public static final String INTEGER = "Integer";

	String getTablename();
	
	String getName();
	
	String getType();

	int getSize();
	
	int getNullable();
	
}
