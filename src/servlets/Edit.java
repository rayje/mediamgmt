package servlets;

import helpers.AddHelper;
import helpers.GetHelper;
import helpers.RequestHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.Media;
import database.MediaValue;
import database.Table;
import exceptions.NoSchemaException;

/**
 * Servlet implementation class Edit
 */
@WebServlet("/Edit")
public class Edit extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private static final String ID = "id";
	private static final String RESULTS_JSP = "results.jsp";
	private static final String ADMIN_JSP = "admin.jsp";
	private static final String EDIT_JSP = "edit.jsp";
	private static final String MEDIA_VALUES = "mediaValues";
	private static final String MEDIA_ID = "mediaId";
	private static final String ERROR_MESSAGE = "errorMessage";
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		GetHelper helper = (GetHelper) request.getServletContext().getAttribute(GetHelper.SERVICE_NAME);
		try {
			helper.getTagDescriptions(request);
		} catch (NoSchemaException e) {
			request.setAttribute(ERROR_MESSAGE, "You must first upload your schema files");
			
			RequestDispatcher dispatcher = request.getRequestDispatcher(ADMIN_JSP);
			dispatcher.forward(request, response);
			
			return;
		}
		
		// Find Media by id
		String id = request.getParameter(ID);
		Media media = helper.find(id);
		
		// Filter Non-Editable fields
		List<MediaValue> values = new ArrayList<MediaValue>();
		for (MediaValue value : media.getValues()) {
			if (!Table.NAMES.contains(value.getName())) {
				values.add(value);
			}
		}
		
		request.setAttribute(MEDIA_ID, media.getId());
		request.setAttribute(MEDIA_VALUES, values);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(EDIT_JSP);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AddHelper helper = (AddHelper) request.getServletContext().getAttribute(AddHelper.SERVICE_NAME);
		String mediaId = request.getParameter(MEDIA_ID);
		
		try {
			RequestHelper.updateData(mediaId, request.getParameterMap(), helper);
		} catch (IllegalStateException e) {
			e.printStackTrace();
			
			response.sendError(HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE);
			return;
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(RESULTS_JSP);
		dispatcher.forward(request, response);
	}

}
