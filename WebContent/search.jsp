<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
			<meta charset='utf-8'>
			<title>Project 8 (search)</title>
			
			<link href="css/style.css" rel="stylesheet" type="text/css">
			
			<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
			<script type="text/javascript">
				$(function() {
				    $("input.search").click(function(event) {
						var id = this.id;
						$('.visible')
							.addClass('hidden')
							.removeClass('visible');
						
						$('#' + id + '-search')
							.removeClass('hidden')
							.addClass('visible');
						
						$('#search-type').val(id);
				    });
				    
				    $("#addBtn").click(function(event) {
				    	$("#search-text")
				    		.append("<input class=\"input\" type=\"text\" name=\"search-text\" size=\"50\"/><br/>");
				    	
				    	return false;
				    });
				});
			</script>
		
	</head>
	<body>
		<%@include file="header.jsp"%>
	
		<div class="body">
			<div class="page">
				<div class="page-header">
					<span>Search Media</span>
				</div>
				<div id="type-form" class="inner-container">
					<span class="container-header">Search</span>
					<hr/>
					<div>
						<span class="label">Choose search type</span>
						<br/><br/>
						<input class="search" id="tag" name="select" type="radio">By Tag<br/>
						<input class="search" id="group" name="select" type="radio">By Profile<br/>
						<input class="search" id="mediaid" name="select" type="radio">By Media Id<br/>
						<input class="search" id="fulltext" name="select" type="radio">By Full Text Search<br/>
					</div>
				</div>
				
				<form class="search-form" action="Search" method="post">
				
					<input id="search-type" type="hidden" name="search-type"/>
				
					<div id="tag-search" class="inner-container hidden">
						<span class="container-header">By Tag</span>
						<hr/>
						<span class="label">Enter tag</span><br/>
						<div>
							<select id="tag_select" name="tagName" class="select" id="main-select">
								<c:forEach items="${tagDescriptions}" var="tagDesc">
									<option value="${ tagDesc['tablename'] }:${ tagDesc['name'] }">${ tagDesc['name'] }</option>
								</c:forEach>
							</select>
							<input class="input" type="text" name="tagValue" size="50"/><br/>
						</div>
						<br/>
						
						<div style="margin-top: 15px;">
							<p>
								<input type="submit" value="Submit" class="btn"/>
							</p>
						</div>
					</div>
					
					<div id="group-search" class="inner-container hidden">
						<span class="container-header">By Profile</span>
						<hr/>
						<c:if test="${not empty profiles }">
							<div class="inner-container data-item">
								<span class="container-header">Profiles</span>
								<hr/>
								<c:if test="${not empty message }">
									${message }
								</c:if>
								
								<div id="tags">
									<c:forEach items="${profiles}" var="profile" varStatus="counter">
										<div class="profile">
											<input type="radio" name="profile" value="${ profile['_id'] }">  Search by Profile ${counter.index + 1}
											<c:forEach var="entry" items="${profile}">
												<c:if test="${ entry.key != '_id' }">
													<div class="profile-value">
														<span class="label">Key: </span>${entry.key}<br/>
														<span class="label">Value: </span>${entry.value }
													</div>
												</c:if>
												<hr/>
											</c:forEach>
										</div>
									</c:forEach>
									
									<div style="margin-top: 15px;">
										<p>
											<input type="submit" value="Submit" class="btn"/>
										</p>
									</div>
								</div>
							</div>
						</c:if>
					</div>
					
					<div id="mediaid-search" class="inner-container hidden">
						<span class="container-header">By Media Id</span>
						<hr/>
						<div>
							<span>Enter Media Id</span><br/><br/>
							<span class="label">Media Id: </span><input class="input" type="text" name="mediaid" size="100"/><br/> 
						</div>
						
						<div style="margin-top: 15px;">
							<p>
								<input type="submit" value="Submit" class="btn"/>
							</p>
						</div>
					</div>
					
					<div id="fulltext-search" class="inner-container hidden">
						<span class="container-header">By Full Text Search</span>
						<hr/>
						<div>
							<p>
								This search form will use Full-Text search to search across
								the description of the meta-data.
							</p>
							<br/>
							<span class="label">Enter search text</span><br/><br/>
							<span class="label">Search Text: </span>&nbsp;&nbsp;&nbsp;&nbsp;<button id="addBtn" class="smallBtn">Add</button><br/>
							<div id="search-text">
								<input class="input" type="text" name="search-text" size="50"/><br/>
							</div>
						</div>
						
						<div style="margin-top: 15px;">
							<p>
								<input type="submit" value="Submit" class="btn"/>
							</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>