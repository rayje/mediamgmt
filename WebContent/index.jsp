<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<title>Project 8</title>
		
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<%@include file="header.jsp" %>
		
		<div class="body">
			<div class="page">
				<div class="page-header">
					<span>Media Database</span>
				</div>
			
				<div class="inner-container">
					<p>
					This is the main page for the Media Database Application.
					</p>
					<p>
					At startup, the database is not populated with any tables. You
					will need to upload two schemas (Standard and Domain) in the
					form of an XSD. 
					Upload schemas on the <a href="Admin">Admin Page</a>.
					</p>
					
					<hr/>
					<p>
					Once tables are loaded, you can add media to the database,
					and provided tags that allow search of your media within the 
					database.
					</p>
					
					<hr/>
					<p>
					The following table describes the links on the header of this page.
					</p>
					<table>
						<tr>
							<th width="100">Link</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>
								<a href="Admin"><span class="label">Admin</span></a>
							</td>
							<td>
								<p>
									The Admin page can be used to uploaed schema files
									or add search profiles to the database.
								</p>
							</td>
						</tr>
						<tr>
							<td>
								<a href="Add"><span class="label">Add</span></a>
							</td>
							<td>
								<p>
									The Add page is used to add new Media files to the
									database and to create tags to make the Media file
									searchable.
								</p>
							</td>
						</tr>
						<tr>
							<td>
								<a href="Search"><span class="label">Search</span></a>
							</td>
							<td>
								<p>
									The Search page provides functionality to search the
									database for Media files by one of four methods:
								</p>
								<ol>
									<li>Tag: Search by a single tag.</li>
									<li>Profile: Search by comparison with Profiles.</li>
									<li>Media Id: Search by similar media to a Media Id.</li>
									<li>Full Text Search: Search by using Full Text Search.</li>
								</ol>
							</td>
						</tr>
						<tr>
							<td>
								<a href="Tables"><span class="label">Tables</span></a>
							</td>
							<td>
								<p>
									The Tables pages provides a way to view the tables in the 
									database.
								</p>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>