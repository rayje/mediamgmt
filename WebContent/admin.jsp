<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<title>Project 8</title>
		
		<link href="css/style.css" rel="stylesheet" type="text/css">
	
	</head>
	<body>
		<%@include file="header.jsp" %>
		
		<div class="body">
			<div class="page">
				<div class="page-header">
					<span>Admin Page</span>
				</div>
				
				<c:if test="${not empty errorMessage}">
					<div class="errorMsg">
						<p>
						${errorMessage }
						</p>
					</div>
				</c:if>
			
				<p>
					This page should be used to upload a schema file to used to generate the
					database that will contains the tags for all media files.
				</p>
			
				<div class="inner-container">
					<span class="container-header">Upload schema files</span>
					<hr/>
				
					<form name="upload" action="Admin" enctype="multipart/form-data" method="post">
						<p>
							<span class="label">Standard Schema File: </span><br/>
							<input type="file" name="standard" /><br/><br/>
							
							<span class="label">Domain Schema File: </span><br/>
							<input type="file" name="domain" />
						</p>
						<br/>
						<p>
							<input type="submit" value="Upload" class="btn"/>
						</p>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>