package schema;

public class BasicElement implements SchemaElement{

	private String name;
	private String type;
	
	public BasicElement(String name, String type) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		if (type.equals("xsd:string")) {
			return "VARCHAR";	
		} else if (type.equals("xsd:dateTime")) {
			return "TIMESTAMP";
		} else if (type.equals("xsd:positiveInteger") || type.equals("xsd:integer")) {
			return "INTEGER";
		}
		
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
