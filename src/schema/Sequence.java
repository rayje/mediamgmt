package schema;

import java.util.ArrayList;
import java.util.List;

public class Sequence {

	private List<SchemaElement> elements = new ArrayList<SchemaElement>();
	
	public void addElement(SchemaElement e) {
		elements.add(e);
	}
	
	public List<SchemaElement> getElements() {
		return elements;
	}
	
}
