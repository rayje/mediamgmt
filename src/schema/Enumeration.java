package schema;

public class Enumeration implements SchemaElement {

	private String value;
	
	public Enumeration(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
}
