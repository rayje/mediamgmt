package listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class AttrListener
 *
 */
@WebListener
public class AttrListener implements ServletContextAttributeListener {

    public void attributeAdded(ServletContextAttributeEvent event) {
    	ServletContext ctx = event.getServletContext();
    	
    	ctx.log("Attribute Added: " + event.getName());
    }

    public void attributeReplaced(ServletContextAttributeEvent event) {
    	ServletContext ctx = event.getServletContext();
    	
    	ctx.log("Attribute Replaced: " + event.getName());
    }

    public void attributeRemoved(ServletContextAttributeEvent event) {
    	ServletContext ctx = event.getServletContext();
    	
    	ctx.log("Attribute Removed: " + event.getName());
    }
	
}
