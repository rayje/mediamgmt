package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Table {
	
	public static final String STANDARD = "standard";
	
	public static final String DOMAIN = "domain";
	
	public static final List<String> NAMES = Arrays.asList(new String[] {"mediaid", "id", "standard_id"});

	private String name;
	private final Map<String, Column> colMap;
	
	private final Database db;
	
	public Table(String name, Database db) {
		this.name = name;
		this.db = db;
		colMap = new HashMap<String, Column>();
	}
	
	public String getName() {
		return name;
	}
	
	public void addColumn(Column col) {
		if (col instanceof ForeignColumn) {
			Column column = (Column) colMap.get(col.getName());
			
			if (column == null) {
				System.out.println(col.getName() + " not in map yet");
				return;
			}
			
			if (column instanceof ParentColumn) {
				ForeignColumn fc = (ForeignColumn) col;
				fc.setParentColumn((ParentColumn) column);
			}
		}
		
		colMap.put(col.getName(), col);
	}
	
	public Collection<Column> getColumns() {
		return Collections.unmodifiableCollection(colMap.values());
	}
	
	public Column getColumn(String name) {
		return colMap.get(name);
	}
	
	public int insert(Map<String, String> data) throws SQLException, ParseException {
		Set<String> keys = data.keySet();
		String sql = prepareSql(keys);

		
		PreparedStatement stmt = db.prepareStatement(sql.toString());

		try {
			int index = 1;
			for (String key : keys) {
				String value = data.get(key);
				Column col = getColumn(key);
	
				if (col == null) {
					System.out.println("Col does not exist: " + key);
				}
				if (col.getType().equals(Column.STRING)) {
					stmt.setString(index++, value);
				} else if (col.getType().equals(Column.INTEGER)) {
					stmt.setInt(index++, Integer.parseInt(value));
				} else if (col.getType().equals(Column.TIMESTAMP)) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
					Date parsedDate = dateFormat.parse(value);
					stmt.setTimestamp(index++, new Timestamp(parsedDate.getTime()));
				}
			}
	
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				return rs.getInt(1);
			}
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return -1;
	}
	
	private String prepareSql(Set<String> keys) {
		StringBuilder colNames = new StringBuilder();
		StringBuilder params = new StringBuilder();

		for (String key : keys) {
			if (colNames.length() > 0) {
				colNames.append(",");
				params.append(",");
			}
			colNames.append(key);
			params.append("?");
		}

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ");
		sql.append(name);
		sql.append(" (");
		sql.append(colNames);
		sql.append(") VALUES (");
		sql.append(params);
		sql.append(")");
		
		return sql.toString();
	}
	
	public int update(String id, Map<String, String> data, String type) throws SQLException, ParseException {
		Set<String> keys = data.keySet();
		String sql = prepareUpdateSql(id, keys, type.equals(STANDARD) ? "mediaid" : "standard_id");
		System.out.println(sql);

		PreparedStatement stmt = db.prepareStatement(sql);
		try {
			int index = 1;
			for (String key : keys) {
				String value = data.get(key);
				Column col = getColumn(key);
	
				if (col == null) {
					System.out.println("Col does not exist: " + key);
				}
				if (col.getType().equals(Column.STRING)) {
					stmt.setString(index++, value);
				} else if (col.getType().equals(Column.INTEGER)) {
					stmt.setInt(index++, Integer.parseInt(value));
				} else if (col.getType().equals(Column.TIMESTAMP)) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
					Date parsedDate = dateFormat.parse(value);
					stmt.setTimestamp(index++, new Timestamp(parsedDate.getTime()));
				}
			}
	
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				return rs.getInt(1);
			}
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return -1;
	}
	
	private String prepareUpdateSql(String mediaId, Set<String> cols, String key) {
		StringBuilder settings = new StringBuilder();

		for (String col : cols) {
			if (settings.length() > 0) {
				settings.append(", ");
			}
			
			settings.append(col);
			settings.append(" =");
			settings.append(" ?");
		}

		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ");
		sql.append(name);
		sql.append(" set ");
		sql.append(settings.toString());
		sql.append(" where ");
		sql.append(key);
		sql.append(" = '");
		sql.append(mediaId);
		sql.append("'");
		
		return sql.toString();
	}
}
