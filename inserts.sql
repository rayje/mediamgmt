INSERT INTO ALLOWED_TYPES (allowed_type) values ('image'), ('audio'), ('video');

INSERT INTO LANGUAGES (language) values ('en'), ('es'), ('ja'), ('de'), ('zh'), ('pt');

INSERT INTO RATING_VALUES (rating) values ('G'), ('PG'), ('PG-13'), ('R'), ('NC-17'), ('None');

INSERT INTO FILM_TYPES (film_type) values ('Short Film'),('Full Feature'), ('Trailer'), ('Series'), ('None');

