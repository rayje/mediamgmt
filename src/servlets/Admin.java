package servlets;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import database.Database;
import database.Table;

/**
 * Servlet implementation class Upload
 */
@WebServlet("/Admin")
@MultipartConfig
public class Admin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static String getFilename(Part p) {
		String h = p.getHeader("content-disposition");
		String[] sections = h.split("\\s*;\\s*");
		
		for (String s : sections) {
			if (s.startsWith("filename=")) {
				return s.substring(9).replace("\"", "");
			}
		}
		
		return null;
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
		dispatcher.forward(request, response);
	}
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Part standardPart = null;
		Part domainPart = null;
		
		try {
			standardPart = request.getPart("standard");
			domainPart = request.getPart("domain");
		} catch (IllegalStateException e) {
			response.sendError(HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE);
			return;
		}
		
		if (standardPart.getSize() == 0 || domainPart.getSize() == 0) {
			String part = standardPart.getSize() == 0 ? "Standard" : "Domain"; 
			request.setAttribute("errorMessage", part + " schema is required");
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		InputStream standardStream = standardPart.getInputStream();
		InputStream domainStream = domainPart.getInputStream();
		
		if (standardStream == null || domainStream == null) {
			request.setAttribute("errorMessage", "Both Domain and Standard Schema XSD files are required");
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
			dispatcher.forward(request, response);
		}
		
		Database db = (Database) request.getServletContext().getAttribute(Database.SERVICE_NAME);
		db.load(standardStream, Table.STANDARD);
		db.load(domainStream, Table.DOMAIN);
		db.initTables();
		
		request.setAttribute("message", "Media: Schema Created");
		request.setAttribute("page_header", "Create results");
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("results.jsp");
		dispatcher.forward(request, response);
	}

}
