package helpers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.Database;

public class RemoveHelper {

	public static final String SERVICE_NAME = "RemoveHelper";
	
	private final Database db;
	
	public RemoveHelper(Database db) {
		this.db = db;
	}
	
	public void remove(String mediaId) throws SQLException {
		Statement stmt = db.getStatement();
		
		String sql = "select id from standard where mediaid = '" + mediaId + "'";
		System.out.println(sql);
		ResultSet rs = stmt.executeQuery(sql);
		while (rs.next()) {
			int id = rs.getInt(1);
			
			String deleteDomain = "delete from domain where standard_id = " + id;
			stmt.executeUpdate(deleteDomain);
			
			String deleteStandard = "delete from standard where id = " + id;
			stmt.executeUpdate(deleteStandard);
		}
		
		stmt.close();
		
		db.removeFile(mediaId);
	}
	
}
