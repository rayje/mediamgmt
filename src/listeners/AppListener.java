package listeners;

import helpers.AddHelper;
import helpers.GetHelper;
import helpers.ProfileHelper;
import helpers.RemoveHelper;
import helpers.SearchHelper;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import database.Database;

/**
 * Application Lifecycle Listener implementation class AppListener
 *
 */
@WebListener
public class AppListener implements ServletContextListener {

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent event) {
        Database db = new Database();
        db.initTables();
        
        ServletContext ctx = event.getServletContext();
        ctx.setAttribute(Database.SERVICE_NAME, db);
        ctx.setAttribute(AddHelper.SERVICE_NAME, new AddHelper(db));
        ctx.setAttribute(GetHelper.SERVICE_NAME, new GetHelper(db));
        ctx.setAttribute(RemoveHelper.SERVICE_NAME, new RemoveHelper(db));
        ctx.setAttribute(ProfileHelper.SERVICE_NAME, new ProfileHelper(db));
        ctx.setAttribute(SearchHelper.SERVICE_NAME, new SearchHelper());
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent event) {
        Database db = (Database) event.getServletContext().getAttribute(Database.SERVICE_NAME);
        db.close();
    }
	
}
