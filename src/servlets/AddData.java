package servlets;

import helpers.AddHelper;
import helpers.GetHelper;
import helpers.RequestHelper;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import database.Media;
import exceptions.NoSchemaException;

/**
 * Servlet implementation class AddData
 */
@WebServlet("/Add")
@MultipartConfig
public class AddData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		GetHelper helper = (GetHelper) request.getServletContext().getAttribute(GetHelper.SERVICE_NAME);
		try {
			helper.getTagDescriptions(request);
		} catch (NoSchemaException e) {
			request.setAttribute("errorMessage", "You must first upload your schema files");
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
			dispatcher.forward(request, response);
			
			return;
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("add.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Part mediaPart = request.getPart("media_file");
		if (mediaPart == null) {
			request.getServletContext().log("media file is null");
			request.setAttribute("errorText", "media file is null");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		AddHelper helper = (AddHelper) request.getServletContext().getAttribute(AddHelper.SERVICE_NAME);
		Media media = null;
		
		try {
			media = RequestHelper.addData(mediaPart, request.getParameterMap(), helper);
		} catch (IllegalStateException e) {
			e.printStackTrace();
			
			response.sendError(HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE);
			return;
		}
		
		request.setAttribute("mediaItems", Collections.singletonList(media));
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("results.jsp");
		dispatcher.forward(request, response);
	}
	
	
}
