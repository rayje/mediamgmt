package schema;

import java.util.ArrayList;
import java.util.List;

public class Restriction {

	private String base;
	
	private List<Enumeration> enumerations;
	
	public Restriction(String base) {
		this.base = base;
		
		enumerations = new ArrayList<Enumeration>();
	}
	
	public void addEnumeration(Enumeration e) {
		enumerations.add(e);
	}
	
	public String getBase() {
		return base;
	}
	
	
}
