package database;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;

import schema.XsdParser;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

public class Database {

	private static final String NULLABLE2 = "NULLABLE";
	private static final String COLUMN_SIZE = "COLUMN_SIZE";
	private static final String DATA_TYPE = "DATA_TYPE";
	private static final String COLUMN_NAME = "COLUMN_NAME";
	public static final String SERVICE_NAME = "DatabaseService";

	private Connection conn;
	private Map<String, Table> tables;
	
	private MongoClient client;
	private DB mongoDb;
	private GridFS gridFS;

	public Database() {
		try {
			Class.forName("org.postgresql.Driver");
			client = new MongoClient("127.0.0.1", 27017);
			mongoDb = client.getDB("media");
			gridFS = new GridFS(mongoDb);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		String url = "jdbc:postgresql://localhost/media";
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void initTables() {
		tables = new HashMap<String, Table>();
		List<Table> allTables = getTables();
		
		try {
			DatabaseMetaData md = conn.getMetaData();
			for (Table table : allTables) {
				// Over write existing parent with foreign cols
				List<Column> parentCols = getParentColumns(md, table.getName());
				for (Column col : parentCols) {
					table.addColumn(col);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Generates a PreparedStatement that will return generated keys from an
	 * insert.
	 * 
	 * @param sql
	 *            The SQL string for the prepared statement.
	 * @return An instance of a PreparedStatement.
	 * @throws SQLException
	 */
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
	}
	
	/**
	 * Gets an instance of a Statement.
	 * 
	 * @return An instance of a Statement.
	 * @throws SQLException
	 */
	public Statement getStatement() throws SQLException {
		return conn.createStatement();
	}
	
	/**
	 * Stores the media file in MongoDb.
	 * 
	 * @param mediaStream
	 *            An InputStream that contains the bytes of the media file
	 *            content.
	 * @param filename
	 *            The name of the input file.
	 * @return An Object representing the media id.
	 */
	public Object storeFile(InputStream mediaStream, String filename) {
		GridFSInputFile mediaFile = gridFS.createFile(mediaStream, filename);
		mediaFile.save();
		
		return mediaFile.getId();
	}
	
	/**
	 * Retrieves the file content from MongoDb using GridFS.
	 * 
	 * @param out An instance of OutputStream used to write the data.
	 * @param id The id of the file in MongoDb.
	 * @throws IOException
	 */
	public void retrieveFile(OutputStream out, String id) throws IOException {
		ObjectId oid = new ObjectId(id);
		
		GridFSDBFile mediaFile = gridFS.find(oid);
		mediaFile.writeTo(out);
	}
	
	public void removeFile(String id) {
		gridFS.remove(new ObjectId(id));
	}
	
	/**
	 * Retrieves the filename from MongoDb.
	 * 
	 * @param id
	 *            The id of the file stored in MongoDb.
	 * @return A String representing the filename of the media file stored in
	 *         the media database.
	 */
	public String retrieveFilename(String id) {
		ObjectId oid = new ObjectId(id);
		
		GridFSDBFile mediaFile = gridFS.find(oid);
		if (mediaFile == null) {
			return null;
		}
		
		return mediaFile.getFilename();
	}

	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void load(InputStream is, String type) {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			XsdParser parser = new XsdParser();
			
			Map<String, List<String>> ddlMap = parser.parseToDdl(is, type);
			List<String> ddl = ddlMap.get(XsdParser.DDL);
			
			for (String statement : ddl) {
				stmt.executeUpdate(statement);
			}
			
			List<String> inserts = ddlMap.get(XsdParser.INSERTS);
			for (String insert : inserts) {
				int result = stmt.executeUpdate(insert);
				System.out.println(result);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<Table> getTables() {
		DatabaseMetaData md;
		List<Table> tables = new ArrayList<Table>();

		try {
			md = conn.getMetaData();

			String[] types = { "TABLE" };
			ResultSet resultSet = md.getTables(null, null, "%", types);

			while (resultSet.next()) {
				String tableName = resultSet.getString(3);
				Table table = getTable(tableName);
				tables.add(table);
				
				this.tables.put(tableName, table);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return tables;
	}
	
	/**
	 * Gets a table description if it exists.
	 * 
	 * @param tableName
	 *            The name of the table to get.
	 * @return An instance of Table.
	 * @throws SQLException
	 */
	public Table getTable(String tableName) throws SQLException {
		Table table = tables.get(tableName);
		if (table != null ) {
			return table;
		}
		
		for (String key : tables.keySet()) {
			if (key.toLowerCase().equals(tableName.toLowerCase())) {
				table = tables.get(key);
				
				if (table != null) {
					return table;
				}
			}
		}
		
		DatabaseMetaData md = conn.getMetaData();
		return getTable(md, tableName);
	}
	
	/**
	 * Gets a table description from the database if it exists.
	 * 
	 * @param md
	 *            An instance of DatabaseMetaData
	 * @param tableName
	 *            The name of the table to query from the database.
	 * @return An instance of Table.
	 */
	public Table getTable(DatabaseMetaData md, String tableName) {
		Table table = new Table(tableName, this);
		boolean tableExists = false;
		ResultSet rs;
		try {
			rs = md.getColumns(null, null, tableName, null);
			while (rs.next()) {
				tableExists = true;
				
				String name = rs.getString(COLUMN_NAME);
				int dataType = rs.getInt(DATA_TYPE);
				int size = rs.getInt(COLUMN_SIZE);
				int nullable = rs.getInt(NULLABLE2);

				ParentColumn col = new ParentColumn(name, dataType, size, nullable, tableName);
				table.addColumn(col);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (!tableExists) {
			return null;
		}
		
		tables.put(tableName, table);
		return table;
	}
	
	public List<Column> getParentColumns(DatabaseMetaData md, String tableName) throws SQLException {
		List<Column> columns = new ArrayList<Column>();
		
		for (Map.Entry<String, Table> entry : tables.entrySet()) {
			Table table = entry.getValue();
			ResultSet rs = md.getCrossReference(null, null, table.getName(), null, null, tableName);
			while (rs.next()) {
				String fkColumnName = rs.getString("FKCOLUMN_NAME");
				String pkTableName = rs.getString("PKTABLE_NAME");
				
				String sql = "select * from " + pkTableName;
				Statement stmt = getStatement();
				
				try {
					ResultSet results = stmt.executeQuery(sql);
					
					ForeignColumn col = new ForeignColumn(fkColumnName, pkTableName);
					while (results.next()) {
						int id = results.getInt(1);
						String value = results.getString(2);
						col.addValue(id, value);
					}
					
					columns.add(col);
				} finally {
					if (stmt != null) {
						try {
							stmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		return columns;
	}
	
	/**
	 * Adds a profile to MondoDB.
	 * 
	 * @param profile
	 */
	public void addProfile(Profile profile) {
		DBCollection coll = mongoDb.getCollection("profiles");
		if (coll == null) {
			coll = mongoDb.createCollection("profiles", null);
			coll.setObjectClass(Profile.class);
		}
		
		coll.insert(profile);
	}
	
	/**
	 * Gets the list if existing profiles.
	 * 
	 * @return
	 */
	public List<Profile> getProfiles() {
		DBCollection coll = mongoDb.getCollection("profiles");
		if (coll == null) {
			return Collections.emptyList();
		}
		coll.setObjectClass(Profile.class);
		
		List<Profile> profiles = new ArrayList<Profile>();
		DBCursor cursor = coll.find();
		for (Iterator<DBObject> it = cursor.iterator(); it.hasNext();) {
			Profile p = (Profile) it.next();
			profiles.add(p);
		}
		
		return profiles;
	}

	public Profile getProfile(String id) {
		DBCollection coll = mongoDb.getCollection("profiles");
		if (coll == null) {
			return null;
		}
		coll.setObjectClass(Profile.class);
		
		DBObject idObj = new BasicDBObject("_id", new ObjectId(id));
		return (Profile) coll.findOne(idObj);
	}

	public int removeProfile(String id) {
		DBCollection coll = mongoDb.getCollection("profiles");
		if (coll == null) {
			return 0;
		}
		coll.setObjectClass(Profile.class);
		
		DBObject idObj = new BasicDBObject("_id", new ObjectId(id));
		WriteResult result = coll.remove(idObj);
		
		return result.getN();
	}
}
	