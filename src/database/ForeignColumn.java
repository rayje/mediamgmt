package database;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ForeignColumn implements Column {

	private Map<String, Integer> values;
	
	private String name;
	private String parentTable;
	private ParentColumn parentColumn;
	
	public ForeignColumn(String name, String parentTable) {
		this.name = name;
		this.parentTable = parentTable;
		
		values = new HashMap<String, Integer>();
	}
	
	public void setParentColumn(ParentColumn col) {
		parentColumn = col;
	}

	public void addValue(int id, String value) {
		values.put(value, id);
	}
	
	public Map<String, Integer> getValues() {
		return Collections.unmodifiableMap(values);
	}
	
	public String getParentTable() {
		return parentTable;
	}
	
	public String getTablename() {
		return parentColumn.getTablename();
	}
	
	public ParentColumn getParentColumn() {
		return parentColumn;
	}
	
	public String getName() {
		return name;
	}
	
	public String getType() {
		if (parentColumn != null)
			return parentColumn.getType();
		else 
			return "None";
	}

	public int getSize() {
		if (parentColumn != null)
			return parentColumn.getSize();
		else
			return -1000000;
	}

	public int getNullable() {
		if (parentColumn != null)
			return parentColumn.getNullable();
		else
			return -1000000;
	}
}
