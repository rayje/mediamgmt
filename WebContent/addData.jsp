<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
			<meta charset='utf-8'>
			<title>Project 8 (Add)</title>
			
			<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
			<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
			<script type="text/javascript" src="scripts/media.js"></script>
			
			<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
			<link href="css/style.css" rel="stylesheet" type="text/css">
			
			<script>
				$(function() {
					$( ".datepicker" ).datepicker();
				});
			</script>
	</head>
	<body>
		<%@include file="header.jsp"%>
	
		<div class="body">
			<div class="page">
				<div class="page-header">
					<span>Add Media</span>
				</div>
				
				<form name="upload" action="Add" class="add-form" enctype="multipart/form-data" method="post">
					<div class="inner-container data-item">
						<span class="container-header">Media File</span>
						<hr/>
						<p>
							Upload File: <input type="file" name="media_file" />
						</p>
						<br/>
					</div>
				
					<div class="inner-container data-item">
						<span class="container-header">Standard Tags</span>
						<hr/>
						<span class="label">Media Type: </span>
						<select name="standard:media_type">
						  <option value="1">image</option>
						  <option value="2">audio</option>
						  <option value="3">video</option>
						</select><br/>
						<span class="label">Media Size: </span><input class="input" type="text" name="standard:media_size"/><br/>
						<span class="label">Creator: </span><input class="input" type="text" name="standard:creator"/><br/>
						<span class="label">Uploaded By: </span><input class="input" type="text" name="standard:uploaded_by"/><br/>
						<span class="label">Upload Date: </span><input class="input datepicker" type="text" name="standard:uploaddate"/><br/>
						<span class="label">Language: </span>
						<select name="standard:language">
						  <option value="1">en</option>
						  <option value="2">es</option>
						  <option value="3">ja</option>
						  <option value="4">de</option>
						  <option value="5">zh</option>
						  <option value="5">pt</option>
						</select><br/>
						<span class="label">Publication Date: </span><input class="input datepicker" type="text" name="standard:publicationdate"/><br/>
						<span class="label">Format: </span><input class="input" type="text" name="standard:format"/><br/>
						<span class="label">Title: </span><input class="input" type="text" name="standard:title"/><br/>
						<span class="label">Subject: </span><input class="input" type="text" name="standard:subject"/><br/>
						<span class="label">License: </span><input class="input" type="text" name="standard:license"/><br/>
					</div>
					
					<div class="inner-container data-item">
						<span class="container-header">Domain Tags</span>
						<hr/>
						<span class="label">Rating: </span>
						<select name="domain:rating">
						  <option value="1">G</option>
						  <option value="2">PG</option>
						  <option value="3">PG-13</option>
						  <option value="4">R</option>
						  <option value="5">NC-17</option>
						  <option value="6">None</option>
						</select><br/>
						<span class="label">Film Type: </span>
						<select name="domain:film_type">
						  <option value="1">Short Film</option>
						  <option value="2">Full Feature</option>
						  <option value="3">Trailer</option>
						  <option value="4">Series</option>
						  <option value="5">None</option>
						</select><br/>
						<span class="label">Category: </span><input class="input" type="text" name="domain:category"/><br/>
						<span class="label">Genre: </span><input class="input" type="text" name="domain:genre"/><br/>
						<span class="label">Description: </span><br/>
						<textarea class="input" name="domain:description" rows="4" cols="135">
						</textarea><br/>
						<span class="label">Likes: </span><input class="input" type="text" name="domain:likes"/><br/>
						<span class="label">Dislikes: </span><input class="input" type="text" name="domain:dislikes"/><br/>
						<span class="label">Height: </span><input class="input" type="text" name="domain:height"/><br/>
						<span class="label">Width: </span><input class="input" type="text" name="domain:width"/><br/>
						<span class="label">Caption: </span><input class="input" type="text" name="domain:caption" size="120"/><br/>
						<span class="label">Quality: </span><input class="input" type="text" name="domain:quality"/><br/>
						<span class="label">Duration: </span><input class="input" type="text" name="domain:duration"/><br/>
						<span class="label">Episode: </span><input class="input" type="text" name="domain:episode"/><br/>
					</div>
					
					<div style="margin-top: 15px;">
						<p>
							<input type="submit" value="Submit" class="btn"/>
						</p>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>