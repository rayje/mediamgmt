<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<title>Project 8 (search)</title>
		
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<%@include file="header.jsp"%>
	
		<div class="body">
			<div class="page">
				<c:forEach items="${tables}" var="table">
					<div class="inner-container">
						<h2>Table: ${table['name']}</h2><br/><br/>
						<table>
							<tr>
								<th>Name</th>
								<th>Type</th>
								<th>Size</th>
								<th>Nullable</th>
							</tr>
								<c:forEach items="${table['columns']}" var="col">
									<tr>
										<td>${col['name']}</td>
										<td>${col['type']}</td>
										<td>${col['size']}</td>
										<td>${col['nullable']}</td>	
									</tr>
								</c:forEach>
						</table>
					</div>
				</c:forEach>
			</div>
		</div>
	</body>
</html>