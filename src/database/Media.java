package database;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Media {

	private String id;
	
	private String filename;
	
	private List<MediaValue> values;
	
	public Media(String id, String filename) {
		this.id = id;
		this.filename = filename;
		
		values = new ArrayList<MediaValue>();
	}

	public String getId() {
		return id;
	}

	public String getFilename() {
		return filename;
	}
	
	public void addValue(MediaValue value) {
		values.add(value);
	}

	public List<MediaValue> getValues() {
		return Collections.unmodifiableList(values);
	}
}
