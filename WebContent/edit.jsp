<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<title>Project 8 (Edit)</title>
			
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script type="text/javascript" src="scripts/media.js"></script>
		
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<%@include file="header.jsp"%>
		
		<div class="hidden" id="tag_template">
			<select name="tag_name" class="select">
				<c:forEach items="${tagDescriptions}" var="tagDesc">
					<option value="${ tagDesc['tablename'] }:${ tagDesc['name'] }">${ tagDesc['name'] }</option>
				</c:forEach>
			</select>
			<input class="input" type="text" name="tag" size="50"/>
			<button class="smallBtn addBtn">+</button>
		</div>
	
		<div class="body">
			<div class="page">
				<div class="page-header">
					<span>Edit Media</span>
				</div>
				
				<form name="edit" action="Edit" class="edit-form" method="post">
					<input type="hidden" name="mediaId" value="${mediaId}" />
					
					<div class="inner-container data-item">
						<span class="container-header">Tags</span>
						<hr/>
						<div id="tags">
							<c:forEach items="${mediaValues}" var="mediaValue">
								<div>
									<c:set var="mediaName" value="${mediaValue['name']}"/>
									<c:set var="tagName" value="${mediaValue['tablename']}:${mediaName}"/>
									<c:set var="tagValue" value="${mediaValue['value']}" />
									<c:set var="tagType" value="${mediaValue['type'] }"/>
									
									<select id="tag_select" name="tag_name" class="select">
										<option value="${ mediaValue['tablename'] }:${mediaName}" selected>${mediaName}</option>
										
										<c:forEach items="${tagDescriptions}" var="tagDesc">
											<c:if test="${tagDesc['name'] != mediaName}">
													<option value="${ tagDesc['tablename'] }:${ tagDesc['name'] }">${ tagDesc['name'] }</option>
											</c:if>
										</c:forEach>
									</select>
									
									<c:choose>
										<c:when test="${ tagType == 'Enum' }">
											<select id="value_select" name="${tagName}" class="select">
												<option value="${mediaValue['indexValue']}">${mediaValue['value']}</option>
												
												<c:forEach items="${mediaValue['allowedValues']}" var="value" varStatus="counter">
													<c:if test="${ value != tagValue && counter.index > 0}" >
														<option value="${counter.index}">${ value }</option>
													</c:if>
												</c:forEach>
											</select>
										</c:when>
										<c:when test="${ tagType == 'Timestamp' }">
											<input class="input datepicker" type="text" name="${tagName}" size="50" value="${tagValue}"/>
										</c:when>
										<c:otherwise>
											<input class="input" type="text" name="${tagName}" size="50" value="${tagValue}"/>
										</c:otherwise>
									</c:choose>
									<button class="smallBtn addBtn" id="addBtn">+</button>
									<button class="smallBtn removeBtn" id="removeBtn">x</button>
								</div>
							</c:forEach>
						</div>
					</div>
					
					<div style="margin-top: 15px;">
						<p>
							<input type="submit" value="Submit" class="btn"/>
						</p>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>