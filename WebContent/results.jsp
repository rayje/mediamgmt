<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<title>Project 8</title>
		
		<link href="css/style.css" rel="stylesheet" type="text/css">
		
	</head>
	<body>
		<%@include file="header.jsp" %>
		
		<div class="body">
			<div class="page">
				<div class="page-header">
					<span>${ page_header }</span>
				</div>
				
				<c:choose>
				    <c:when test="${not empty message}">
				        <span class="label">${message}</span>
				    </c:when>
				    <c:otherwise>
				    	<span class="label">Total Results: </span>${fn:length(mediaItems)}<br/>
				    
				        <c:forEach items="${mediaItems}" var="item">
							<div class="inner-container">
								<span class="label">Name: </span>${item['filename']}<br/>
								<span class="label">Media Id: </span><a href="Download?id=${item['id']}">${item['id']}</a>
								<div style="margin-top: 15px;">
									<a href="Edit?id=${item['id']}" class="btn">Edit</a>
									<a href="Delete?id=${item['id']}" class="btn">Delete</a>
								</div> 
							</div>
						</c:forEach>
				    </c:otherwise>
				</c:choose>
				
				
			</div>
		</div>
	</body>
</html>