<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<title>Project 8 (Add)</title>
			
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script type="text/javascript" src="scripts/media.js"></script>
		
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<%@include file="header.jsp"%>
		
		<div class="hidden" id="tag_template">
			<select name="tag_name" class="select">
				<c:forEach items="${tagDescriptions}" var="tagDesc">
					<option value="${ tagDesc['tablename'] }:${ tagDesc['name'] }">${ tagDesc['name'] }</option>
				</c:forEach>
			</select>
			<input class="input" type="text" name="tag" size="50"/>
			<button class="smallBtn">+</button>
		</div>
	
		<div class="body">
			<div class="page">
				<div class="page-header">
					<span>Profiles</span>
				</div>
				
				<c:if test="${not empty message}">
					<div class="message">
						<p>
						${message }
						</p>
					</div>
				</c:if>
				
				<div class="inner-container data-item">
					<span class="container-header">Add Profile</span>
					<hr/>
					<form name="profiles" action="Profiles" method="post">
						<div id="tags">
							<div id="tag_template">
								<select id="tag_select" name="tag_name" class="select">
									<c:forEach items="${tagDescriptions}" var="tagDesc">
										<option value="${ tagDesc['tablename'] }:${ tagDesc['name'] }">${ tagDesc['name'] }</option>
									</c:forEach>
								</select>
								<input class="input" type="text" name="tag" size="50"/>
								<button class="smallBtn addBtn">+</button>
							</div>
						</div>
						
						<div style="margin-top: 15px;">
							<p>
								<input type="submit" value="Submit" class="btn"/>
							</p>
						</div>
					</form>
				</div>
				
				<c:if test="${not empty profiles }">
					<div class="inner-container data-item">
						<span class="container-header">Profiles</span>
						<hr/>
						<div id="tags">
							<c:forEach items="${profiles}" var="profile">
								<div class="profile">
									<input type="hidden" name="id" value="${ profile['_id'] }" />
									<c:forEach var="entry" items="${profile}">
										<c:if test="${ entry.key != '_id' }">
											<div class="profile-value">
												<span class="label">Key: </span>${entry.key}<br/>
												<span class="label">Value: </span>${entry.value }
											</div>
										</c:if>
										<hr/>
									</c:forEach>
									<div style="margin-top: 15px;">
										<p>
											<a href="Delete?profile=${profile['_id'] }" class="btn">Delete</a>	
										</p>
									</div>
								</div>
							</c:forEach>
						</div>
						
						
					</div>
				</c:if>
			</div>
		</div>
	</body>
</html>